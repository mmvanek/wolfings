# README #

This is the wolfings.com code.

### Tools ###

First you must install all this stuff.

* [Google App Engine](https://developers.google.com/appengine/downloads#Google_App_Engine_SDK_for_Python) to run the code on your machine
* [Plan9port](https://code.google.com/p/plan9port/downloads/list) mainly for `mk`
* [Sass](http://sass-lang.com/install) to make the CSS files


### Running the code ###

1. Set up the App Engine SDK to run this application
2. In a terminal window, run `mk clean && mk` from the application's base directory. Whenever you change SCSS files, run `mk` again.


### Organization ###

* **style/css**: CSS files that the user is served
* **style/scss**: SCSS files that build into CSS
* **handlers**: webapp2 request handlers
* **jinja2**: Page templates
* **models**: NDB interfaces
* **static**: Images and javascript


### Request Handlers ###
For a full list of handlers and the corresponding paths, see main.py.